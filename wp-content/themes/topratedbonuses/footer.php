<footer>
    <?php $footer = get_field('footer', 7); ?>
    <div class="footer-menu-background">
        <div class="footer-menus">
            <div class="footer-menu">
                <?php echo wp_nav_menu(array('menu' => 'Footer menu')); ?>
            </div>
            <div class="footer-icons">
                <?php foreach ($footer['footer_icon'] as $icon) : ?>
                    <div class="footer-icon">
                        <a href="<?php echo $icon['svg_link']['url']; ?>" target="_blank"><?php echo $icon['svg']; ?></a>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="footer-text">
                <p><?php echo $footer['footer_text']; ?></p>
            </div>
            <div class="copyright">
                <p><?php echo $footer['copyright']; ?></p>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>