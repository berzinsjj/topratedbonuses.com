<?php /* Template Name: Info */ ?>
<?php get_header(); ?>
    <div class="page-container">
        <div class="info-text-background">
            <div class="page-info-text">
                <?php $page = get_post(get_the_ID()); ?>
                <h1><?php echo $page->post_title; ?></h1>
            </div>
        </div>
        <div class="page-content">
            <div class="page-info">
                <div class="page-info-paragraph">
                    <?php echo nl2br($page->post_content); ?>
                </div>
                <?php if (!empty(get_field('banner_bottom')['affialite_link'] && get_field('banner_bottom')['logo'])) : ?>
                    <div class="featured-banner">
                        <div class="featured">
                            <p>FEATURED</p>
                        </div>
                        <?php $banner_bottom = get_field('banner_bottom'); ?>
                        <div class="banner">
                            <a href="<?php echo $banner_bottom['affialite_link']; ?>" target="_blank"><img src="<?php echo $banner_bottom['logo']; ?>"></a>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>