jQuery(function ($) {
    jQuery(document).ready(function () {

        // Mobile menu
        $(".hamburger").click(function () {
            $(this).toggleClass("is-active");
            $(".menu-main-menu-container").toggleClass("is-active");
        });

        // Slider
        $('.slider-container-casino').slick({
            dots: false,
            arrows: true,
            infinite: true,
            prevArrow:"<svg class=\"prev\" version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n" +
                "\t viewBox=\"0 0 512.006 512.006\" style=\"enable-background:new 0 0 512.006 512.006;\" xml:space=\"preserve\">\n" +
                "<g>\n" +
                "\t<g>\n" +
                "\t\t<path d=\"M388.419,475.59L168.834,256.005L388.418,36.421c8.341-8.341,8.341-21.824,0-30.165s-21.824-8.341-30.165,0\n" +
                "\t\t\tL123.586,240.923c-8.341,8.341-8.341,21.824,0,30.165l234.667,234.667c4.16,4.16,9.621,6.251,15.083,6.251\n" +
                "\t\t\tc5.461,0,10.923-2.091,15.083-6.251C396.76,497.414,396.76,483.931,388.419,475.59z\"/>\n" +
                "\t</g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "</svg>",
            nextArrow:"<svg class=\"next\" version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n" +
                "\t viewBox=\"0 0 512.005 512.005\" style=\"enable-background:new 0 0 512.005 512.005;\" xml:space=\"preserve\">\n" +
                "<g>\n" +
                "\t<g>\n" +
                "\t\t<path d=\"M388.418,240.923L153.751,6.256c-8.341-8.341-21.824-8.341-30.165,0s-8.341,21.824,0,30.165L343.17,256.005\n" +
                "\t\t\tL123.586,475.589c-8.341,8.341-8.341,21.824,0,30.165c4.16,4.16,9.621,6.251,15.083,6.251c5.461,0,10.923-2.091,15.083-6.251\n" +
                "\t\t\tl234.667-234.667C396.759,262.747,396.759,249.264,388.418,240.923z\"/>\n" +
                "\t</g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "<g>\n" +
                "</g>\n" +
                "</svg>",
            autoplay: 5000,
            slidesToShow: 1,
            slidesToScroll: 1,
            adaptiveHeight: true
        });

        // Heart Click
        $(".casino-heart").click(function () {
            // parvers stringu skaitlii, atrod klasi
            var counter = parseInt($(this).find(".like-number").html());
            // iedod ID ,atdrod casino pec ID
            var casino_id = $(this).attr("id");
            // nolasa localstorage
            var local_storage_content = localStorage.getItem("clicked_casino");
            var clicked_casino;
            // taisa ifu
            if (local_storage_content === null) {
                // izveido tuksu masiivu
                clicked_casino = [];
            } else {
                // panem no localstorage , parse izveido masivu
                clicked_casino = JSON.parse(local_storage_content);
            }

            var dublicated_clicked_casino = false;
                // iet cauri clicked_casino masiivam,
            $.each( clicked_casino, function( key, value ) {
                // saliidzina vai value vienads ar casino_id
                if (value === casino_id) {
                    dublicated_clicked_casino = true;
                }
            });

            if (dublicated_clicked_casino === false) {
                // iemet skaitli localstorage
                clicked_casino.push(casino_id);
                // izveido un saglabaa key, value
                localStorage.setItem("clicked_casino", JSON.stringify(clicked_casino));
                // Ja ir jau uzklikots, tad nonem nost ID
            } else {
                var index = clicked_casino.indexOf(casino_id);
                if (index > -1) {
                    clicked_casino.splice(index, 1);
                }
                localStorage.setItem("clicked_casino", JSON.stringify(clicked_casino));
            }

            var clicks = counter;
            $(this).toggleClass('heart');
            $(this).hasClass("heart") ? clicks++ : clicks--;
            $(this).find(".like-number").html(clicks);
        });

        // Heart click onload
        loadClickedCasinos();
    });
});

function loadClickedCasinos() {
    // 1. Dabuut no localstorage klikotos kaziishus
    var local_storage_content = localStorage.getItem("clicked_casino");
    var clicked_casino = JSON.parse(local_storage_content);
    // 2. Jaataisa foreach kas iet cauri visiem klikotajiem kaziihsiem

    $.each( clicked_casino, function( key, value ) {
        var current_casino = parseInt($("#" + value).find(".like-number").html());
        $("#" + value).find(".like-number").html(current_casino + 1);
        $("#" + value).toggleClass('heart');
    });
}
