<?php /* Template Name: Bonuses */ ?>
<?php get_header(); ?>
<div class="page-container">
    <?php if (!empty(get_field('banner_top'))) : ?>
        <div class="banner-background">
            <div class="featured-banner">
                <div id="featured">
                    <p>FEATURED</p>
                </div>
                <?php $banner_top = get_field('banner_top'); ?>
                <div class="banner">
                    <a href="<?php echo $banner_top['affialite_link']; ?>" target="_blank"><img src="<?php echo $banner_top['logo']; ?>"></a>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="page-content page-bonuses">
        <?php $first_text = get_field('first_text'); ?>
        <?php foreach ($first_text as $text) : ?>
            <div class="first-text">
                <h1><?php echo $text['title']; ?></h1>
                <div class="date">
                    <p><?php echo $text['date_text']; ?> <span id="datetime"><?php echo date("F Y"); ?></span></p>
                </div>
            </div>
            <div class="first-paragraph">
                <?php echo nl2br($text['text']); ?>
            </div>
        <?php endforeach; ?>
        <?php if (!empty(get_field('featured_casinos'))) : ?>
            <div class="casino-container">
                <?php $featured_casinos = get_field('featured_casinos'); ?>
                <?php foreach ($featured_casinos as $company) : ?>
                    <div class="oj-oj">
                        <div class="casino-box">
                            <div class="casino-logo">
                                <a href="<?php echo get_field('affialite_link', $company->ID); ?>" target="_blank"> <img src="<?php echo get_field('logo', $company->ID); ?>"></a>
                            </div>
                            <div class="casino-content">
                                <div class="name-heart-choice">
                                    <div class="casino-name">
                                        <p><?php echo $company->post_title; ?></p>
                                    </div>
                                    <div class="casino-heart" id="<?php echo $company->ID; ?>">
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 176.1 151.9" style="enable-background:new 0 0 176.1 151.9;" xml:space="preserve"><g><g><path d="M150.4,6.2c-7.1-3.9-15.3-6.2-24-6.2C111,0,97.2,7,88.1,18C78.9,7,65.1,0,49.7,0c-8.7,0-16.9,2.3-24,6.2C10.4,14.7,0,31,0,49.8c0,5.4,0.9,10.5,2.5,15.3c8.5,38.4,85.6,86.8,85.6,86.8s77-48.4,85.6-86.8c1.6-4.8,2.5-10,2.5-15.3C176.1,31,165.7,14.7,150.4,6.2z"/></g></g></svg>
                                        <p class="like-number"><?php echo get_field('like_number', $company->ID); ?></p>
                                    </div>
                                    <?php if (!empty(get_field('red_bonus', $company->ID))) : ?>
                                        <div class="casino-choice">
                                            <p><?php echo get_field('red_bonus', $company->ID); ?></p>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <div class="players-appreciate">
                                    <p>Players Appreciate:</p>
                                </div>
                                <?php foreach (get_field('tags', $company->ID) as $tag) : ?>
                                    <div class="casino-list-svg">
                                        <div class="casino-svg-list">
                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 344.963 344.963" style="enable-background:new 0 0 344.963 344.963;" xml:space="preserve"><g><path d="M321.847,86.242l-40.026-23.11l-23.104-40.02h-46.213l-40.026-23.11l-40.026,23.11H86.239l-23.11,40.026L23.11,86.242v46.213L0,172.481l23.11,40.026v46.213l40.026,23.11l23.11,40.026h46.213l40.02,23.104l40.026-23.11h46.213l23.11-40.026l40.026-23.11v-46.213l23.11-40.026l-23.11-40.026V86.242H321.847z M156.911,243.075c-3.216,3.216-7.453,4.779-11.671,4.72c-4.219,0.06-8.455-1.504-11.671-4.72l-50.444-50.444c-6.319-6.319-6.319-16.57,0-22.889l13.354-13.354c6.319-6.319,16.57-6.319,22.889,0l25.872,25.872l80.344-80.35c6.319-6.319,16.57-6.319,22.889,0l13.354,13.354c6.319,6.319,6.319,16.57,0,22.889L156.911,243.075z"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>
                                        </div>
                                        <ul>
                                            <li><?php echo $tag['tag'] ?></li>
                                        </ul>
                                    </div>
                                <?php endforeach; ?>
                                <div class="new-customer-offer">
                                    <p>New Customer Offer:</p>
                                </div>
                                <div class="casino-bonus">
                                    <a href="<?php echo get_field('affialite_link', $company->ID); ?>" target="_blank"><?php echo get_field('offer', $company->ID); ?></a>
                                </div>
                            </div>
                            <div class="casino-rating-button">
                                <div class="rating-and-number-container">
                                    <div class="stars">
                                        <?php if (get_field('rating', $company->ID) == 0) : ?>
                                            <div class="stars-container">
                                                <div class="star">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                                <div class="star">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                                <div class="star">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                                <div class="star">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                                <div class="star">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                            </div>
                                        <?php elseif (get_field('rating', $company->ID) == 1) : ?>
                                            <div class="stars-container">
                                                <div class="star full">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                                <div class="star">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                                <div class="star">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                                <div class="star">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                                <div class="star">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                            </div>
                                        <?php elseif (get_field('rating', $company->ID) == 2) : ?>
                                            <div class="stars-container">
                                                <div class="star full">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                                <div class="star full">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                                <div class="star">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                                <div class="star">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                                <div class="star">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                            </div>
                                        <?php elseif (get_field('rating', $company->ID) == 3) : ?>
                                            <div class="stars-container">
                                                <div class="star full">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                                <div class="star full">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                                <div class="star full">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                                <div class="star">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                                <div class="star">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                            </div>
                                        <?php elseif (get_field('rating', $company->ID) == 4) : ?>
                                            <div class="stars-container">
                                                <div class="star full">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                                <div class="star full">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                                <div class="star full">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                                <div class="star full">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                                <div class="star">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                            </div>
                                        <?php elseif (get_field('rating', $company->ID) == 5) : ?>
                                            <div class="stars-container">
                                                <div class="star full">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                                <div class="star full">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                                <div class="star full">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                                <div class="star full">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                                <div class="star full">
                                                    <svg version="1.1" id="box-stars-us" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.2 14.2" style="enable-background:new 0 0 14.2 14.2;" xml:space="preserve"><path d="M11.8,0H2.4C1.1,0,0,1.1,0,2.4v9.4c0,1.3,1.1,2.4,2.4,2.4h9.4c1.3,0,2.4-1.1,2.4-2.4V2.4C14.2,1.1,13.1,0,11.8,0z M10,11.8L7.1,9.6l-2.9,2.2l1.1-3.6L2.4,6H6l1.1-3.6L8.2,6h3.6L8.9,8.2L10,11.8z"/></svg>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    <div class="number">
                                        <p><?php echo get_field('rating', $company->ID); ?><span>/5</span></p>
                                    </div>
                                </div>
                                <div class="reputation">
                                    <p>Reputation:
                                        <?php if (get_field('rating', $company->ID) == 5) : ?>
                                            <span class="rep-perfect">Perfect</span>
                                        <?php elseif (get_field('rating', $company->ID) == 4) : ?>
                                            <span class="rep-verygood">Very Good</span>
                                        <?php elseif (get_field('rating', $company->ID) == 3) : ?>
                                            <span class="rep-good">Good</span>
                                        <?php elseif (get_field('rating', $company->ID) == 2) : ?>
                                            <span class="rep-questionable">Questionable</span>
                                        <?php elseif (get_field('rating', $company->ID) == 1 || get_field('rating', $company->ID) == 0) : ?>
                                            <span class="rep-poor">Poor</span>
                                        <?php endif; ?>
                                    </p>
                                </div>
                                <div class="casino-button">
                                    <a href="<?php echo get_field('affialite_link', $company->ID); ?>" target="_blank">Sign Up</a>
                                </div>
                                <div class="casino-terms">
                                    <a href="<?php echo get_field('terms_link', $company->ID); ?>" target="_blank">18+. T&C’s apply</a>
                                </div>
                            </div>
                        </div>
                        <?php if (!empty(get_field('terms_info', $company->ID))) : ?>
                            <div class="casino-terms-info">
                                <a href="<?php echo get_field('terms_link', $company->ID); ?>" target="_blank"><?php echo get_field('terms_info', $company->ID); ?></a>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="paragraph-container page-casino">
        <?php $paragraph = get_field('paragraph'); ?>
        <?php foreach ($paragraph as $text) : ?>
            <div class="paragraph-text">
                <?php echo nl2br($text['text']); ?>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="page-content page-x">
        <?php if (!empty(get_field('banner_bottom')['logo'])) : ?>
            <div class="featured-banner">
                <div class="featured">
                    <p>FEATURED</p>
                </div>
                <?php $banner_top = get_field('banner_bottom'); ?>
                <div class="banner">
                    <a href="<?php echo $banner_top['affialite_link']; ?>" target="_blank"><img src="<?php echo $banner_top['logo']; ?>"></a>
                </div>
            </div>
        <?php endif; ?>
        <?php $second_paragraph = get_field('second_paragraph'); ?>
        <?php foreach ($second_paragraph as $text) : ?>
            <div class="paragraph-text">
                <?php echo nl2br($text['text']); ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<?php get_footer(); ?>
